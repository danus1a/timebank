﻿using System.Collections.Generic;

namespace TimeBankWebApplication.Helpers
{
    public class Pager
    {
        public string PreviousPage { get; set; }
        public string NextPage { get; set; }
        public List<string> Pages { get; set; }
        public string CurrentPage { get; set; }
        public string Action { get; set; }
        public string RouteCategory { get; set; }
        public string Controller { get; set; }
    }
}
