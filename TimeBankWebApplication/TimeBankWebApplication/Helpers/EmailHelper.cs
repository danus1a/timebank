﻿using MailKit.Net.Smtp;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using TimeBankWebApplication.Models;
using TimeBankWebApplication.ViewModels;

namespace TimeBankWebApplication.Helpers
{
    public class EmailHelper
    {
        private string FromAddress { get; set; }
        private string Password { get; set; }
        private string FromAdressTitle = "TimeBank notification";
        private string SmtpServer = "smtp.gmail.com";
        private int SmtpPortNumber = 587;
        public string ToAddress { get; set; }
        public string ToAdressTitle { get; set; }
        public string Subject { get; set; }
        public string BodyContent { get; set; }

        public EmailHelper(IOptions<AppSettings> settings)
        {
            FromAddress = settings.Value.Email;
            Password = settings.Value.Password;
        }

        public void SendContactForm(ContactViewModel contact)
        {
            ToAddress = FromAddress;
            Subject = $"{contact.Name} have sent a question from TimeBank portal";
            BodyContent = $"<div><b>Email</b> - {contact.Email}</div>" +
                $"<div><b>Message</b> - {contact.Message}</div>";

            Send();
        }

        public void SendServiceNotification(Service service)
        {
            Subject = "You have created a TimeBank service";
            BodyContent = $"<h2>Your new service's name is - {service.Caption}.</h2>" +
                $"<h3>From now you will receive notifications about this serfice.</h3>" +
                $"<div><b>Category</b> - {service.Category.Name}</div>" +
                $"<div><b>Description</b> - {service.Description}</div>" +
                $"<div><b>Credits number</b> - {service.Credits}</div>" +
                $"<div><b>Date</b> - {service.DateCreated}</div>";

            Send();
        }

        public void SendRequestNotification(Request request)
        {
            Subject = "You have created a TimeBank request";
            BodyContent = $"<h2>Your request's caption is - {request.Caption}.</h2>" +
                $"<h3>When someone wish to help - you will receive a notification.</h3>" +
                $"<div><b>Category</b> - {request.Category.Name}</div>" +
                $"<div><b>Description</b> - {request.Description}</div>" +
                $"<div><b>Credits number</b> - {request.Credits}</div>" +
                $"<div><b>Date created</b> - {request.DateCreated}</div>" +
                $"<div><b>Requested date</b> - {request.DateEnd}</div>";

            Send();
        }

        public void SendRequestOrderNotification(Order order)
        {
            //send to owner
            Subject = "You have received a response for TimeBank request";
            BodyContent = $"<h2>To contact with request's handler go to your personal cabinet.</h2>" +
                $"<h3>Request - {order.Request.Caption}</h3>" +
                $"<div><b>Category</b> - {order.Request.Category.Name}</div>" +
                $"<div><b>Description</b> - {order.Request.Description}</div>" +
                $"<div><b>Credits number</b> - {order.Request.Credits}</div>" +
                $"<div><b>Date created</b> - {order.Request.DateCreated}</div>" +
                $"<div><b>Requested date</b> - {order.Request.DateEnd}</div>" +
                $"<div><b>User phone</b> - {order.User.Phone}</div>" +
                $"<div><b>User address</b> - {order.User.Address}</div>";

            Send();

            //send to handler
            ToAddress = order.User.Email;
            ToAdressTitle = $"{order.User.FirstName} {order.User.LastName}";
            Subject = "You have responded on a TimeBank request";
            BodyContent = $"<h2>To contact with request's owner go to your personal cabinet.</h2>" +
                $"<h3>Request - {order.Request.Caption}</h3>" +
                $"<div><b>Category</b> - {order.Request.Category.Name}</div>" +
                $"<div><b>Description</b> - {order.Request.Description}</div>" +
                $"<div><b>Credits number</b> - {order.Request.Credits}</div>" +
                $"<div><b>Date created</b> - {order.Request.DateCreated}</div>" +
                $"<div><b>Requested date</b> - {order.Request.DateEnd}</div>" +
                $"<div><b>User phone</b> - {order.Request.User.Phone}</div>" +
                $"<div><b>User address</b> - {order.Request.User.Address}</div>";

            Send();
        }

        public void SendServiceOrderNotification(Order order)
        {
            //send to owner
            Subject = "You have received a response for TimeBank service";
            BodyContent = $"<h2>To contact with this person go to your personal cabinet.</h2>" +
                $"<h3>Service - {order.Service.Caption}</h3>" +
                $"<div><b>Category</b> - {order.Service.Category.Name}</div>" +
                $"<div><b>Description</b> - {order.Service.Description}</div>" +
                $"<div><b>Credits number</b> - {order.Service.Credits}</div>" +
                $"<div><b>Date created</b> - {order.Service.DateCreated}</div>" +
                $"<div><b>User phone</b> - {order.User.Phone}</div>" +
                $"<div><b>User address</b> - {order.User.Address}</div>"; 

            Send();

            //send to receiver
            ToAddress = order.User.Email;
            ToAdressTitle = $"{order.User.FirstName} {order.User.LastName}";
            Subject = "You have responded on a TimeBank service";
            BodyContent = $"<h2>To contact with service's owner go to your personal cabinet.</h2>" +
                $"<h3>Service - {order.Service.Caption}</h3>" +
                $"<div><b>Category</b> - {order.Service.Category.Name}</div>" +
                $"<div><b>Description</b> - {order.Service.Description}</div>" +
                $"<div><b>Credits number</b> - {order.Service.Credits}</div>" +
                $"<div><b>Date created</b> - {order.Service.DateCreated}</div>" +
                $"<div><b>User phone</b> - {order.Service.User.Phone}</div>" +
                $"<div><b>User address</b> - {order.Service.User.Address}</div>";

            Send();
        }
        public void SendServiceOrderNotificationDelete(Order order)
        {
            //send to owner
            Subject = "The user have refused your TimeBank service";
            BodyContent = $"<h2>User {order.User.FirstName} {order.User.LastName} have refused your service.</h2>" +
                $"<h3>Service - {order.Service.Caption}</h3>" +
                $"<div><b>Category</b> - {order.Service.Category.Name}</div>" +
                $"<div><b>Description</b> - {order.Service.Description}</div>" +
                $"<div><b>Credits number</b> - {order.Service.Credits}</div>" +
                $"<div><b>Date created</b> - {order.Service.DateCreated}</div>" +
                $"<div><b>User phone</b> - {order.User.Phone}</div>" +
                $"<div><b>User address</b> - {order.User.Address}</div>"; 

            Send();

            //send to receiver
            ToAddress = order.User.Email;
            ToAdressTitle = $"{order.User.FirstName} {order.User.LastName}";
            Subject = "You have refused a TimeBank service";
            BodyContent = $"<h2>You are not a customer for this service anymore.</h2>" +
                $"<h3>Service - {order.Service.Caption}</h3>" +
                $"<div><b>Category</b> - {order.Service.Category.Name}</div>" +
                $"<div><b>Description</b> - {order.Service.Description}</div>" +
                $"<div><b>Credits number</b> - {order.Service.Credits}</div>" +
                $"<div><b>Date created</b> - {order.Service.DateCreated}</div>" +
                $"<div><b>User phone</b> - {order.Service.User.Phone}</div>" +
                $"<div><b>User address</b> - {order.Service.User.Address}</div>";

            Send();
        }

        public void SendRequestOrderNotificationDelete(Order order)
        {
            //send to owner
            Subject = "The user have refused your TimeBank request";
            BodyContent = $"<h2>User {order.User.FirstName} {order.User.LastName} have refused your request.</h2>" +
                $"<h3>Request - {order.Request.Caption}</h3>" +
                $"<div><b>Category</b> - {order.Request.Category.Name}</div>" +
                $"<div><b>Description</b> - {order.Request.Description}</div>" +
                $"<div><b>Credits number</b> - {order.Request.Credits}</div>" +
                $"<div><b>Date created</b> - {order.Request.DateCreated}</div>" +
                $"<div><b>Requested date</b> - {order.Request.DateEnd}</div>" +
                $"<div><b>User phone</b> - {order.User.Phone}</div>" +
                $"<div><b>User address</b> - {order.User.Address}</div>";

            Send();

            //send to handler
            ToAddress = order.User.Email;
            ToAdressTitle = $"{order.User.FirstName} {order.User.LastName}";
            Subject = "You have refused a TimeBank request";
            BodyContent = $"<h2>You are not a performer for this request anymore.</h2>" +
                $"<h3>Request - {order.Request.Caption}</h3>" +
                $"<div><b>Category</b> - {order.Request.Category.Name}</div>" +
                $"<div><b>Description</b> - {order.Request.Description}</div>" +
                $"<div><b>Credits number</b> - {order.Request.Credits}</div>" +
                $"<div><b>Date created</b> - {order.Request.DateCreated}</div>" +
                $"<div><b>Requested date</b> - {order.Request.DateEnd}</div>" +
                $"<div><b>User phone</b> - {order.Request.User.Phone}</div>" +
                $"<div><b>User address</b> - {order.Request.User.Address}</div>";

            Send();
        }

        public void SendResponseNotification(Response response)
        {
            if (response.Order.ServiceId != null)
            {
                //send to owner of order
                Subject = "The user have left a feedback for your TimeBank service";
                BodyContent = $"<h2>For details go to your personal cabinet.</h2>" +
                    $"<h3>Service - {response.Order.Service.Caption}</h3>" +
                    $"<div><b>Date</b> - {response.DateCreated}</div>" +
                    $"<div><b>You have received credits</b> - {response.Credits}</div>" +
                    $"<div><b>User phone</b> - {response.Order.User.Phone}</div>" +
                    $"<div><b>User address</b> - {response.Order.User.Address}</div>";

                Send();

                //send to handler
                ToAddress = response.Order.User.Email;
                ToAdressTitle = $"{response.Order.User.FirstName} {response.Order.User.LastName}";
                Subject = "You have left a feedback for a TimeBank service";
                BodyContent = $"<h2>For details go to your personal cabinet.</h2>" +
                    $"<h3>Service - {response.Order.Service.Caption}</h3>" +
                    $"<div><b>Date</b> - {response.DateCreated}</div>" +
                    $"<div><b>You have spent credits</b> - {response.Credits}</div>" +
                    $"<div><b>User phone</b> - {response.Order.Service.User.Phone}</div>" +
                    $"<div><b>User address</b> - {response.Order.Service.User.Address}</div>";

                Send();
            }
            else
            {
                //send to owner of request
                Subject = "You have left a feedback for a TimeBank request";
                BodyContent = $"<h2>For details go to your personal cabinet.</h2>" +
                    $"<h3>Request - {response.Order.Request.Caption}</h3>" +
                    $"<div><b>Date</b> - {response.DateCreated}</div>" +
                    $"<div><b>You have spent credits</b> - {response.Credits}</div>" +
                    $"<div><b>User phone</b> - {response.Order.Request.User.Phone}</div>" +
                    $"<div><b>User address</b> - {response.Order.Request.User.Address}</div>";

                Send();

                //send to handler
                ToAddress = response.Order.User.Email;
                ToAdressTitle = $"{response.Order.User.FirstName} {response.Order.User.LastName}";
                Subject = "The user have left a feedback for TimeBank request you've done";
                BodyContent = $"<h2>For details go to your personal cabinet.</h2>" +
                    $"<h3>Request - {response.Order.Request.Caption}</h3>" +
                    $"<div><b>Date</b> - {response.DateCreated}</div>" +
                    $"<div><b>You have received credits</b> - {response.Credits}</div>" +
                    $"<div><b>User phone</b> - {response.Order.User.Phone}</div>" +
                    $"<div><b>User address</b> - {response.Order.User.Address}</div>";

                Send();
            }
        }

        private void Send()
        {
            try
            {
                var mimeMessage = new MimeMessage();
                mimeMessage.From.Add(new MailboxAddress(FromAdressTitle, FromAddress));
                mimeMessage.To.Add(new MailboxAddress(ToAdressTitle, ToAddress));
                mimeMessage.Subject = Subject;
                var bodyBuilder = new BodyBuilder
                {
                    HtmlBody = BodyContent
                };
                mimeMessage.Body = bodyBuilder.ToMessageBody();

                using (var client = new SmtpClient())
                {
                    client.Connect(SmtpServer, SmtpPortNumber, false);
                    // Note: only needed if the SMTP server requires authentication  
                    // Error 5.5.1 Authentication   
                    client.Authenticate(FromAddress, Password);
                    client.Send(mimeMessage);
                    client.Disconnect(true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
