﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using TimeBankWebApplication.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.AspNetCore.Identity;
using System;
using Microsoft.AspNetCore.Mvc;

namespace TimeBankWebApplication
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddEntityFramework()
                .AddEntityFrameworkSqlServer()
                .AddDbContext<ApplicationContext>(options => options.UseSqlServer(Configuration["Data:ConnectionString"]));
            services.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<ApplicationContext, int>()
                .AddDefaultTokenProviders();
            services.Configure<IdentityOptions>(options =>
                options.Password = new PasswordOptions()
                {
                    RequireUppercase = false,
                    RequireDigit = false,
                    RequireNonAlphanumeric = false,
                    RequireLowercase = false
                });

            //services.AddMvc();
            services.AddMvc(options =>
            {
                options.SslPort = 44303;
                options.Filters.Add(new RequireHttpsAttribute());
            });

            services.Configure<AppSettings>(Configuration.GetSection("Data"));
            services.AddSession(options => options.IdleTimeout = TimeSpan.FromDays(1));
        }
        
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseSession();
            app.UseIdentity();
            app.UseFacebookAuthentication(new FacebookOptions
            {
                AppId = Configuration["Facebook:AppId"],
                AppSecret = Configuration["Facebook:AppSecret"],
                Scope = { "email" },
                Fields = { "name", "email" },
                SaveTokens = true,
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
