﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TimeBankWebApplication.Helpers;

namespace TimeBankWebApplication.ViewComponents
{
    [ViewComponent]
    public class Pagination : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync(Pager pager)
        {
            return View(pager);
        }
    }
}
