﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace TimeBankWebApplication.ViewComponents
{
    [ViewComponent]
    public class Request : ViewComponent
    {
        public Request()
        {
        }

        public async Task<IViewComponentResult> InvokeAsync(Models.Request request)
        {
            return View(request);
        }
    }
}
