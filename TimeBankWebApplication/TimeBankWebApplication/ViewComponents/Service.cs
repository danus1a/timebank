﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace TimeBankWebApplication.ViewComponents
{
    [ViewComponent]
    public class Service : ViewComponent
    {
        public Service()
        {
        }

        public async Task<IViewComponentResult> InvokeAsync(Models.Service service)
        {
            return View(service);
        }
    }
}
