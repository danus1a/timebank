﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TimeBankWebApplication.Models;

namespace TimeBankWebApplication.ViewComponents
{
    [ViewComponent]
    public class Categories : ViewComponent
    {
        private ApplicationContext _dbContext;
        private readonly ILogger<Categories> _logger;

        public Categories(ApplicationContext dbContext, ILogger<Categories> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            List<Category> categories = new List<Category>();
            try
            {
                categories = await _dbContext.Categories.ToListAsync();
            }
            catch(Exception ex)
            {
                _logger.LogError($"Categories - InvokeAsync - Error: {ex.Message}");
            }
            return View(categories);
        }
    }
}
