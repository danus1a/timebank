﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using TimeBankWebApplication.Models;

namespace TimeBankWebApplication.ViewComponents
{
    [ViewComponent]
    [Authorize]
    public class CreditsCount : ViewComponent
    {
        private ApplicationContext _dbContext;

        public CreditsCount(ApplicationContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var user = await _dbContext.Users.FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
            return View(user);
        }
    }
}
