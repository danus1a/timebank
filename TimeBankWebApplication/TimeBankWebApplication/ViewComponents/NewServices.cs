﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TimeBankWebApplication.Models;

namespace TimeBankWebApplication.ViewComponents
{
    public class NewServices : ViewComponent
    {
        private ApplicationContext _dbContext;
        private readonly ILogger<NewServices> _logger;

        public NewServices(ApplicationContext dbContext, ILogger<NewServices> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }

        public async Task<IViewComponentResult> InvokeAsync(int numberOfItems)
        {
            List<Models.Service> services = new List<Models.Service>();
            List<User> users = new List<User>();
            try
            {
                if (User.Identity.IsAuthenticated)
                {
                    users = await _dbContext.Users.Include(u => u.Services).ThenInclude(s => s.Category)
                        .Where(u => u.UserName != User.Identity.Name).ToListAsync();
                }
                else
                    users = await _dbContext.Users.Include(u => u.Services).ThenInclude(s => s.Category).ToListAsync();
                services = (from user in users
                            from service in user.Services
                            where service.IsActive
                            orderby service.DateCreated descending
                            select service).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError($"NewServices - InvokeAsync - Error: {ex.Message}");
            }
            return View(services.Take(numberOfItems));
        }
    }
}
