﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using TimeBankWebApplication.Helpers;
using TimeBankWebApplication.Models;
using TimeBankWebApplication.ViewModels;

namespace TimeBankWebApplication.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationContext _dbContext;
        private IOptions<AppSettings> _settings;

        public HomeController(ApplicationContext dbContext, IOptions<AppSettings> settings)
        {
            _dbContext = dbContext;
            _settings = settings;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult Contact(ContactViewModel model)
        {
            if (ModelState.IsValid)
            {
                EmailHelper emailHelper = new EmailHelper(_settings);
                emailHelper.SendContactForm(model);
                TempData["ContactFormSubmitted"] = true;
                return RedirectToAction("Index");
            }
            return View(model);
        }
    }
}
