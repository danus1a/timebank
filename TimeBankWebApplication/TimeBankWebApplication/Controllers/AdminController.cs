﻿using Microsoft.AspNetCore.Mvc;
using TimeBankWebApplication.Models;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;
using Microsoft.Extensions.Logging;

namespace TimeBankWebApplication.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private ApplicationContext _dbContext;
        private IOptions<AppSettings> _settings;
        private readonly ILogger<AccountController> _logger;

        public AdminController(ApplicationContext dbContext, IOptions<AppSettings> settings,
                                 ILogger<AccountController> logger)
        {
            _dbContext = dbContext;
            _settings = settings;
            _logger = logger;
        }
        
        [HttpGet("Admin/Categories/All")]
        public async Task<IActionResult> GetCategories()
        {
            try
            {
                var categories = await _dbContext.Categories.Include(c => c.Services).Include(c => c.Requests).ToListAsync();
                return View(categories);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Admin - GetCategories - Error: {ex.Message}");
                return BadRequest();
            }
        }

        [HttpGet("Admin/Services/All")]
        public async Task<IActionResult> GetServices()
        {
            try
            {
                var services = await _dbContext.Services.Include(s => s.Category).ToListAsync();
                return View(services);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Admin - GetServices - Error: {ex.Message}");
                return BadRequest();
            }
        }

        [HttpGet("Admin/Requests/All")]
        public async Task<IActionResult> GetRequests()
        {
            try
            {
                var requests = await _dbContext.Requests.Include(s => s.Category).ToListAsync();
                return View(requests);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Admin - GetRequests - Error: {ex.Message}");
                return BadRequest();
            }
        }

        [HttpGet("Admin/Categories/Create")]
        public IActionResult CreateCategory()
        {
            return View();
        }
        
        [HttpPost("Admin/Categories/Create")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateCategory(Category model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (!_dbContext.Categories.AnyAsync(category =>
                        category.Name == model.Name).Result)
                    {
                        _dbContext.Categories.Add(model);
                        await _dbContext.SaveChangesAsync();
                        return RedirectToAction("GetCategories");
                    }
                    ModelState.AddModelError("Name", "Name already exists");
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Admin - CreateCategory - Error: {ex.Message}");
                    return BadRequest();
                }
            }
            return View(model);
        }
        
        [HttpGet("Admin/Categories/Edit/{id}")]
        public async Task<IActionResult> EditCategory(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            try
            {
                Category category = await _dbContext.Categories.SingleAsync(m => m.CategoryId == id);
                if (category == null)
                {
                    return NotFound();
                }
                return View(category);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Admin - EditCategory - Error: {ex.Message}");
                return BadRequest();
            }
        }

        [HttpPost("Admin/Categories/Edit/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditCategory(Category model)
        {
            try
            {
                Category category = await _dbContext.Categories.SingleAsync(m => m.CategoryId == model.CategoryId);
                if (category == null)
                {
                    return NotFound();
                }
                if (ModelState.IsValid)
                {
                    if (!_dbContext.Categories.AnyAsync(c =>
                        c.Name == model.Name).Result)
                    {
                        category.Name = model.Name;
                        _dbContext.Update(category);
                        await _dbContext.SaveChangesAsync();
                        return RedirectToAction("GetCategories");
                    }
                    ModelState.AddModelError("Name", "Name already exists");
                }
                return View(category);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Admin - EditCategory - Error: {ex.Message}");
                return BadRequest();
            }
        }

        [HttpGet("Admin/Categories/{id}/Services")]
        public async Task<IActionResult> GetCategoryServices(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            try
            {
                Category category = await _dbContext.Categories.SingleAsync((m => m.CategoryId == id));
                if (category == null)
                {
                    return NotFound();
                }
                var services = await _dbContext.Services.Include(s => s.Category).Where(s => s.CategoryId == id).ToListAsync();
                ViewData["CategoryName"] = category.Name;
                return View("GetServices", services);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Admin - GetCategoryServices - Error: {ex.Message}");
                return BadRequest();
            }
        }

        [HttpGet("Admin/Categories/{id}/Requests")]
        public async Task<IActionResult> GetCategoryRequests(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            try
            {
                Category category = await _dbContext.Categories.SingleAsync((m => m.CategoryId == id));
                if (category == null)
                {
                    return NotFound();
                }
                var requests = await _dbContext.Requests.Include(s => s.Category).Where(s => s.CategoryId == id).ToListAsync();
                ViewData["CategoryName"] = category.Name;
                return View("GetRequests", requests);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Admin - GetCategoryRequests - Error: {ex.Message}");
                return BadRequest();
            }
        }

        [HttpGet("Admin/Categories/Delete/{id}")]
        public async Task<IActionResult> DeleteCategory(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            try
            {
                Category category = await _dbContext.Categories.SingleAsync(m => m.CategoryId == id);
                if (category == null)
                {
                    return NotFound();
                }

                return View(category);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Admin - DeleteCategory - Error: {ex.Message}");
                return BadRequest();
            }
        }
        
        [HttpPost("Admin/Categories/Delete/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteCategory(int id)
        {
            try
            {
                Category category = await _dbContext.Categories.SingleAsync(m => m.CategoryId == id);
                _dbContext.Categories.Remove(category);
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Admin - DeleteCategory - Error: {ex.Message}");
            }
            return RedirectToAction("GetCategories");
        }

        [HttpGet("Admin/Services/Delete/{id}")]
        public async Task<IActionResult> DeleteService(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            try
            {
                Service service = await _dbContext.Services.SingleAsync(m => m.ServiceId == id);
                if (service == null)
                {
                    return NotFound();
                }

                return View(service);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Admin - DeleteService - Error: {ex.Message}");
                return BadRequest();
            }
        }

        [HttpPost("Admin/Services/Delete/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteService(int id)
        {
            try
            {
                Service service = await _dbContext.Services.SingleAsync(m => m.ServiceId == id);
                _dbContext.Services.Remove(service);
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Admin - DeleteService - Error: {ex.Message}");
            }
            return RedirectToAction("GetServices");
        }

        [HttpGet("Admin/Requests/Delete/{id}")]
        public async Task<IActionResult> DeleteRequest(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            try
            {
                Request request = await _dbContext.Requests.SingleAsync(m => m.RequestId == id);
                if (request == null)
                {
                    return NotFound();
                }

                return View(request);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Admin - DeleteRequest - Error: {ex.Message}");
                return BadRequest();
            }
        }

        [HttpPost("Admin/Requests/Delete/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteRequest(int id)
        {
            try
            {
                Request request = await _dbContext.Requests.Include(r => r.Orders).SingleAsync(m => m.RequestId == id);
                if (request.Orders.Any())
                {
                    _dbContext.Orders.RemoveRange(request.Orders);
                }
                _dbContext.Requests.Remove(request);
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Admin - DeleteRequest - Error: {ex.Message}");
            }
            return RedirectToAction("GetRequests");
        }

        [HttpGet("Admin/Services/Edit/{id}")]
        public async Task<IActionResult> EditService(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            try
            {
                Service service = await _dbContext.Services.SingleAsync(s => s.ServiceId == id);
                if (service == null)
                {
                    return NotFound();
                }

                return View(service);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Admin - EditService - Error: {ex.Message}");
                return BadRequest();
            }
        }

        [HttpPost("Admin/Services/Edit/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditService(Service model)
        {
            try
            {
                Service service = await _dbContext.Services.SingleAsync(m => m.ServiceId == model.ServiceId);
                if (service == null)
                {
                    return NotFound();
                }
                if (ModelState.IsValid)
                {
                    service.IsActive = false;
                    service.OffReason = model.OffReason;
                    _dbContext.Update(service);
                    await _dbContext.SaveChangesAsync();
                    return RedirectToAction("GetServices");
                }
                return View(service);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Admin - EditService - Error: {ex.Message}");
                return BadRequest();
            }
        }

        [HttpPost("Admin/Services/TurnOn/{id}")]
        public async Task<IActionResult> TurnOnService(int? id)
        {
            try
            {
                Service service = await _dbContext.Services.SingleAsync(m => m.ServiceId == id);
                if (service == null)
                {
                    return NotFound();
                }
                service.IsActive = true;
                service.OffReason = "";
                _dbContext.Update(service);
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Admin - TurnOnService - Error: {ex.Message}");
                return BadRequest();
            }
            return RedirectToAction("GetServices");
        }

        [HttpGet("Admin/Requests/Edit/{id}")]
        public async Task<IActionResult> EditRequest(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            try
            {
                Request request = await _dbContext.Requests.SingleAsync(s => s.RequestId == id);
                if (request == null)
                {
                    return NotFound();
                }
                return View(request);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Admin - EditRequest - Error: {ex.Message}");
                return BadRequest();
            }
        }

        [HttpPost("Admin/Requests/Edit/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditRequest(Request model)
        {
            try
            {
                Request request = await _dbContext.Requests.SingleAsync(m => m.RequestId == model.RequestId);
                if (request == null)
                {
                    return NotFound();
                }
                if (ModelState.IsValid)
                {
                    request.IsActive = model.IsActive;
                    request.OffReason = model.OffReason;
                    _dbContext.Update(request);
                    await _dbContext.SaveChangesAsync();
                    return RedirectToAction("GetRequests");
                }
                return View(request);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Admin - EditRequest - Error: {ex.Message}");
                return BadRequest();
            }
        }

        [HttpPost("Admin/Requests/TurnOn/{id}")]
        public async Task<IActionResult> TurnOnRequest(int? id)
        {
            try
            {
                Request request = await _dbContext.Requests.SingleAsync(m => m.RequestId == id);
                if (request == null)
                {
                    return NotFound();
                }
                request.IsActive = true;
                request.OffReason = "";
                _dbContext.Update(request);
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Admin - EditRequest - Error: {ex.Message}");
            }
            return RedirectToAction("GetRequests");
        }
    }
}
