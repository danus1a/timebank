﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TimeBankWebApplication.Helpers;
using TimeBankWebApplication.Models;

namespace TimeBankWebApplication.Controllers
{
    public class RequestsController : Controller
    {
        private const int _itemsOnPage = 6;
        private const int _pagesOnPagination = 5;
        private ApplicationContext _dbContext;
        private IOptions<AppSettings> _settings;
        private readonly ILogger<AccountController> _logger;

        public RequestsController(ApplicationContext dbContext, IOptions<AppSettings> settings,
                                 ILogger<AccountController> logger)
        {
            _dbContext = dbContext;
            _settings = settings;
            _logger = logger;
        }

        [Authorize]
        public async Task<IActionResult> MyRequests()
        {
            if (User.Identity.IsAuthenticated)
            {
                try
                {
                    var user = await _dbContext.Users.Include(u => u.Requests).ThenInclude(r => r.Orders)
                        .Include(u => u.Requests).ThenInclude(r => r.Category)
                        .FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
                    if (user != null)
                    {
                        return View(user.Requests);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Requests - MyRequests - Error: {ex.Message}");
                    return BadRequest();
                }
                return NotFound();
            }
            return NotFound();
        }

        public async Task<IActionResult> AllRequests(int? page)
        {
            List<Request> requests = new List<Request>();
            List<User> users = new List<User>();
            try
            {
                if (User.Identity.IsAuthenticated)
                {
                    users = await _dbContext.Users
                        .Include(u => u.Requests).ThenInclude(r => r.Category)
                        .Include(u => u.Requests).ThenInclude(r => r.Orders)
                        .Where(u => u.UserName != User.Identity.Name).ToListAsync();
                }
                else
                    users = await _dbContext.Users
                        .Include(u => u.Requests).ThenInclude(r => r.Category)
                        .Include(u => u.Requests).ThenInclude(r => r.Orders)
                        .ToListAsync();
                requests = (from user in users
                            from request in user.Requests
                            where !request.IsDone && request.ActiveOrder == null && request.IsActive
                            select request).ToList();
                return GetPagination(page, requests, "AllRequests", null, false, "AllRequests");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Requests - AllRequests - Error: {ex.Message}");
                return BadRequest();
            }
        }

        [Authorize]
        public async Task<IActionResult> Add()
        {
            if (!User.Identity.IsAuthenticated)
                return BadRequest();
            try
            {
                var user = await _dbContext.Users.FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
                if (user == null)
                    return BadRequest();
                Request request = new Request
                {
                    User = user
                };
                ViewData["CategoryId"] = GetCategoriesSelectList();
                return View(request);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Requests - Add - Error: {ex.Message}");
                return BadRequest();
            }
        }

        private List<SelectListItem> GetCategoriesSelectList(string selectedValue = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            try
            {
                list = _dbContext.Categories.Select(item => new SelectListItem
                {
                    Value = item.CategoryId.ToString(),
                    Text = item.Name,
                    Selected = item.CategoryId.ToString() == selectedValue
                }).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Requests - GetCategoriesSelectList - Error: {ex.Message}");
            }
            return list;
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Add(Request model)
        {
            if (!User.Identity.IsAuthenticated)
                return BadRequest();
            try
            {
                var user = await _dbContext.Users.FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
                if (user == null)
                    return BadRequest();
                model.User = user;
                if (ModelState.IsValid)
                {
                    model.DateCreated = DateTime.Now;
                    model.UserId = user.Id;
                    model.IsActive = true;

                    user.Credits--;

                    _dbContext.Users.Update(user);
                    _dbContext.Requests.Add(model);
                    if (await _dbContext.SaveChangesAsync() > 0)
                    {
                        model = await _dbContext.Requests.Include(s => s.Category).FirstOrDefaultAsync(s => s.RequestId == model.RequestId);
                        EmailHelper emailHelper = new EmailHelper(_settings)
                        {
                            ToAddress = user.Email,
                            ToAdressTitle = $"{user.FirstName} {user.LastName}"
                        };
                        emailHelper.SendRequestNotification(model);
                    }
                    return RedirectToAction("MyRequests");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Requests - Add - Error: {ex.Message}");
            }
            ViewData["CategoryId"] = GetCategoriesSelectList(model.CategoryId.ToString());
            return View(model);
        }

        [Authorize]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            try
            {
                Request request = await _dbContext.Requests.SingleAsync(m => m.RequestId == id);
                if (request == null)
                {
                    return NotFound();
                }
                ViewData["CategoryId"] = GetCategoriesSelectList(request.CategoryId.ToString());

                return View(request);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Requests - Edit - Error: {ex.Message}");
            }
            return BadRequest();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Edit(Request model)
        {
            try
            {
                Request request = await _dbContext.Requests.SingleAsync(m => m.RequestId == model.RequestId);
                if (request == null)
                    return NotFound();
                if (ModelState.IsValid)
                {
                    request.Caption = model.Caption;
                    request.Credits = model.Credits;
                    request.Description = model.Description;
                    request.CategoryId = model.CategoryId;
                    request.DateEnd = model.DateEnd;
                    _dbContext.Update(request);
                    await _dbContext.SaveChangesAsync();
                    return RedirectToAction("MyRequests");
                }
                ViewData["CategoryId"] = GetCategoriesSelectList(request.CategoryId.ToString());
                return View("Edit", model);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Requests - Edit - Error: {ex.Message}");
            }
            return BadRequest();
        }

        [Authorize]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();
            try
            {
                Request request = await _dbContext.Requests.SingleAsync(m => m.RequestId == id);
                if (request == null)
                    return NotFound();
                return View(request);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Requests - Delete - Error: {ex.Message}");
            }
            return BadRequest();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                Request request = await _dbContext.Requests.Include(r => r.User).SingleAsync(m => m.RequestId == id);
                request.User.Credits++;
                request.IsActive = false;
                _dbContext.Requests.Update(request);
                await _dbContext.SaveChangesAsync();
                return RedirectToAction("MyRequests");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Requests - Delete - Error: {ex.Message}");
            }
            return BadRequest();
        }

        [Authorize]
        public async Task<IActionResult> GetRequest(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            try
            {
                Request request = await _dbContext.Requests.Include(r => r.Orders)
                    .ThenInclude(o => o.Response).Include(r => r.Orders)
                    .ThenInclude(o => o.User)
                    .Include(r => r.Category)
                    .FirstOrDefaultAsync(m => m.RequestId == id);
                if (request == null)
                {
                    return NotFound();
                }
                return View(request);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Requests - GetRequest - Error: {ex.Message}");
            }
            return BadRequest();
        }

        private void RenderPagination(int? page, string action, string routeCategory, decimal pagesCount)
        {
            var pager = new Pager
            {
                PreviousPage = page.Value >= 2 ? (page.Value - 1).ToString() : "",
                NextPage = page.Value < pagesCount ? (page.Value + 1).ToString() : "",
                CurrentPage = page.Value.ToString(),
                Action = action,
                RouteCategory = routeCategory,
                Controller = "Requests"
            };
            //current page
            var pagesList = new List<string> { page.Value.ToString() };
            if (page.Value == 1)
            {
                for (var i = page.Value + 1; i <= _pagesOnPagination; i++)
                {
                    if (i > pagesCount)
                        continue;
                    pagesList.Add(i.ToString());
                }
            }
            else if (page.Value == 2)
            {
                pagesList.Add((page.Value - 1).ToString());
                for (var i = page.Value + 1; i <= _pagesOnPagination; i++)
                {
                    if (i > pagesCount)
                        continue;
                    pagesList.Add(i.ToString());
                }
            }
            else if (page.Value == pagesCount)
            {
                for (var i = page.Value - 1; i > 0; i--)
                {
                    if (pagesList.Count == _pagesOnPagination)
                        continue;
                    pagesList.Add(i.ToString());
                }
            }
            else if (page.Value == pagesCount - 1)
            {
                pagesList.Add(pagesCount.ToString());
                for (var i = page.Value - 1; i > 0; i--)
                {
                    if (pagesList.Count == _pagesOnPagination)
                        continue;
                    pagesList.Add(i.ToString());
                }
            }
            else if (page.Value == pagesCount - 2)
            {
                pagesList.Add(pagesCount.ToString());
                pagesList.Add((pagesCount - 1).ToString());
                for (var i = page.Value - 1; i > 0; i--)
                {
                    if (pagesList.Count == _pagesOnPagination)
                        continue;
                    pagesList.Add(i.ToString());
                }
            }
            else
            {
                for (var i = 1; i <= _pagesOnPagination; i++)
                {
                    if (i != page.Value)
                        pagesList.Add(i.ToString());
                }
            }
            pager.Pages = pagesList.OrderBy(item => item).ToList();
            ViewBag.pager = pager;
        }

        private IActionResult GetPagination(int? page, List<Request> requests, string action, string routeCategory, bool isPartial, string viewName)
        {
            if (page == null || page.Value <= 0)
                page = 1;
            decimal pagesCount = Math.Ceiling((decimal)requests.Count / _itemsOnPage);
            RenderPagination(page, action, routeCategory, pagesCount);
            if (!requests.Any())
            {
                if (isPartial)
                    return PartialView(viewName, requests);
                return View(viewName, requests);
            }
            if (isPartial)
                return PartialView(viewName, requests.Skip((page.Value - 1) * _itemsOnPage).Take(_itemsOnPage));
            return View(viewName, requests.Skip((page.Value - 1) * _itemsOnPage).Take(_itemsOnPage));
        }

        public IActionResult PaginationViewComponent(int? page, string action, int routeCategory)
        {
            if (page == null || page.Value <= 0)
                page = 1;
            List<Request> requests = new List<Request>();
            List<User> users = new List<User>();
            try
            {
                if (User.Identity.IsAuthenticated)
                {
                    users = _dbContext.Users.Include(u => u.Requests).Where(u => u.UserName != User.Identity.Name).ToList();
                }
                else
                    users = _dbContext.Users.Include(u => u.Requests).ToList();
                requests = (from user in users
                            from request in user.Requests
                            where request.IsActive
                            select request).ToList();
                if (routeCategory > 0)
                    requests = requests.Where(r => r.CategoryId == routeCategory).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Requests - PaginationViewComponent - Error: {ex.Message}");
            }
            decimal pagesCount = Math.Ceiling((decimal)requests.Count / _itemsOnPage);
            if (page.Value > pagesCount)
                return BadRequest();
            RenderPagination(page, action, routeCategory.ToString(), pagesCount);
            return ViewComponent("Pagination", (Pager)ViewBag.pager);
        }

        public async Task<IActionResult> Category(int? id, int? page)
        {
            if (id == null)
            {
                return BadRequest();
            }
            List<Request> requests = new List<Request>();
            List<User> users = new List<User>();
            try
            {
                var category = _dbContext.Categories.FirstOrDefaultAsync(c => c.CategoryId == id).Result;
                if (category == null && id != 0)
                {
                    return BadRequest();
                }
                if (User.Identity.IsAuthenticated)
                {
                    users = await _dbContext.Users
                        .Include(u => u.Requests).ThenInclude(r => r.Category)
                        .Where(u => u.UserName != User.Identity.Name).ToListAsync();
                }
                else
                    users = await _dbContext.Users
                        .Include(u => u.Requests).ThenInclude(r => r.Category)
                        .ToListAsync();
                requests = (from user in users
                            from request in user.Requests
                            where !request.IsDone && request.ActiveOrder == null && request.IsActive
                            select request).ToList();
                if (id > 0)
                    requests = requests.Where(s => s.CategoryId == id).ToList();
                ViewBag.Title = id > 0 ? category.Name : "All";
                ViewBag.Category = category.Name;
                ViewBag.CategoryId = id;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Requests - AllRequests - Error: {ex.Message}");
            }
            return GetPagination(page, requests, "AllRequests", id.ToString(), true, "RequestsPartialView");
        }
    }
}
