using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using TimeBankWebApplication.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using TimeBankWebApplication.ViewModels;
using System.Linq;
using System.Security.Claims;
using System;

namespace TimeBankWebApplication.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<User> _timeBankManager;
        private readonly SignInManager<User> _loginManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly ApplicationContext _dbContext;
        private readonly ILogger<AccountController> _logger;

        public AccountController(UserManager<User> timeBankManager,
                                 SignInManager<User> loginManager,
                                 RoleManager<Role> roleManager,
                                 ApplicationContext timeBankContext,
                                 ILogger<AccountController> logger)
        {
            _timeBankManager = timeBankManager;
            _loginManager = loginManager;
            _roleManager = roleManager;
            _dbContext = timeBankContext;
            _logger = logger;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register()
        {
            return View();
        }

        private async Task<IdentityResult> AddRole(string roleName)
        {
            return await _roleManager.CreateAsync(new Role { Name = roleName });
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = new User
                    {
                        UserName = model.Login,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        Email = model.Email,
                        Phone = model.Phone,
                        Address = model.Address,
                        Credits = 3
                    };
                    var result = await _timeBankManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        string role = "User";
                        if (user.UserName == "admin")
                        {
                            role = "Admin";
                        }
                        if (!_roleManager.Roles.AnyAsync(r => r.Name == role).Result)
                        {
                            await AddRole(role);
                        }
                        var roleResult = await _timeBankManager.AddToRoleAsync(user, role);
                        if (roleResult.Succeeded)
                        {
                            await _loginManager.SignInAsync(user, isPersistent: false);
                            return RedirectToAction("Index", "Home");
                        }
                    }
                    else
                    {
                        if (result.Errors.Any(e => e.Code.ToLower().Contains("username")))
                        {
                            ModelState.AddModelError("login", result.Errors.FirstOrDefault(e => e.Code.ToLower().Contains("username")).Description);
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Account - Register - Error: {ex.Message}");
                }
            }
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login(string returnUrl = null)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("PersonalCabinet", "Account");
            }
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [Authorize]
        public async Task<IActionResult> PersonalCabinet()
        {
            User user = null;
            try
            {
                user = await _dbContext.Users.FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Account - PersonalCabinet - Error: {ex.Message}");
            }
            var userViewModel = new UserViewModel
            {
                Id = user.Id,
                Address = user.Address,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Phone = user.PhoneNumber
            };
            return View(userViewModel);
        }

        [Authorize]
        public async Task<IActionResult> EditPersonalData()
        {
            User user = null;
            try
            {
                user = await _dbContext.Users.FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Account - EditPersonalData - Error: {ex.Message}");
            }
            var userViewModel = new UserViewModel
            {
                Id = user.Id,
                Address = user.Address,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Phone = user.PhoneNumber
            };
            return View(userViewModel);
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditPersonalData(UserViewModel userViewModel)
        {
            User user = null;
            try
            {
                user = await _dbContext.Users.FirstOrDefaultAsync(u => u.Id == userViewModel.Id);
                if (user == null)
                    return BadRequest();
                if (ModelState.IsValid)
                {
                    user.FirstName = userViewModel.FirstName;
                    user.LastName = userViewModel.LastName;
                    user.PhoneNumber = userViewModel.Phone;
                    user.Email = userViewModel.Email;
                    user.Address = userViewModel.Address;
                    await _dbContext.SaveChangesAsync();
                    return RedirectToAction("PersonalCabinet");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Account - EditPersonalData - Error: {ex.Message}");
            }
            return View(userViewModel);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                try
                {
                    var result = await _loginManager.PasswordSignInAsync(model.Login,
                        model.Password, false, false);
                    if (result.Succeeded)
                    {
                        if (string.IsNullOrWhiteSpace(returnUrl))
                        {
                            return RedirectToAction("Index", "Home");
                        }
                        return RedirectToLocal(returnUrl);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Account - Login - Error: {ex.Message}");
                }
                ModelState.AddModelError("", "Username or password incorrect");
            }
            return View(model);
        }

        public async Task<IActionResult> LogOff()
        {
            if (User.Identity.IsAuthenticated)
            {
                HttpContext.Session.Remove("IsAdmin");
                await _loginManager.SignOutAsync();
            }
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }
        
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult ExternalLogin(string provider, string returnUrl = null)
        {
            // Request a redirect to the external login provider.
            var redirectUrl = Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl });
            var properties = _loginManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
            return Challenge(properties, provider);
        }
        
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ExternalLoginCallback(string returnUrl = null, string remoteError = null)
        {
            if (remoteError != null)
            {
                ModelState.AddModelError(string.Empty, $"Error from external provider: {remoteError}");
                return View(nameof(Login));
            }
            var info = await _loginManager.GetExternalLoginInfoAsync();
            if (info == null)
            {
                return RedirectToAction(nameof(Login));
            }

            // Sign in the user with this external login provider if the user already has a login.
            var result = await _loginManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false);
            if (result.Succeeded)
            {
                _logger.LogInformation(5, "User logged in with {Name} provider.", info.LoginProvider);
                return RedirectToLocal(returnUrl);
            }
            if (result.IsLockedOut)
            {
                return View("Lockout");
            }
            else
            {
                // If the user does not have an account, then ask the user to create an account.
                ViewData["ReturnUrl"] = returnUrl;
                ViewData["LoginProvider"] = info.LoginProvider;
                var email = info.Principal.FindFirstValue(ClaimTypes.Email);
                var firstName = info.Principal.FindFirstValue(ClaimTypes.Name);
                var lastName = info.Principal.FindFirstValue(ClaimTypes.Surname);
                return View("ExternalLoginConfirmation", new RegisterViewModel { Email = email, FirstName = firstName, LastName = lastName });
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ExternalLoginConfirmation(RegisterViewModel model, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await _loginManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new User
                {
                    UserName = model.Login,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Email = model.Email,
                    Phone = model.Phone,
                    Address = model.Address,
                    Credits = 3
                };
                var result = await _timeBankManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    result = await _timeBankManager.AddLoginAsync(user, info);
                    if (result.Succeeded)
                    {
                        string role = "User";
                        if (user.UserName == "admin")
                        {
                            role = "Admin";
                        }
                        if (!_roleManager.Roles.AnyAsync(r => r.Name == role).Result)
                        {
                            await AddRole(role);
                        }
                        var roleResult = await _timeBankManager.AddToRoleAsync(user, role);
                        if (roleResult.Succeeded)
                        {
                            await _loginManager.SignInAsync(user, isPersistent: false);
                            return RedirectToAction("Index", "Home");
                        }
                        AddErrors(roleResult);
                    }
                    AddErrors(result);
                }
                AddErrors(result);
            }

            ViewData["ReturnUrl"] = returnUrl;
            return View(model);
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }
    }
}