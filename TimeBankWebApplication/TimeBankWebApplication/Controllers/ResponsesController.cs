﻿using Microsoft.AspNetCore.Mvc;
using TimeBankWebApplication.Models;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;
using TimeBankWebApplication.Helpers;
using Microsoft.Extensions.Logging;

namespace TimeBankWebApplication.Controllers
{
    public class ResponsesController : Controller
    {
        private ApplicationContext _dbContext;
        private IOptions<AppSettings> _settings;
        private readonly ILogger<AccountController> _logger;

        public ResponsesController(ApplicationContext dbContext, IOptions<AppSettings> settings, ILogger<AccountController> logger)
        {
            _dbContext = dbContext;
            _settings = settings;
            _logger = logger;
        }

        [Authorize]
        public async Task<IActionResult> MyResponses()
        {
            if (User.Identity.IsAuthenticated)
            {
                try
                {
                    var user = await _dbContext.Users.Include(u => u.Orders).ThenInclude(o => o.Response)
                        .Include(d => d.Orders).ThenInclude(f => f.Service)
                        .Include(d => d.Orders).ThenInclude(f => f.Request)
                        .Include(d => d.Requests).ThenInclude(r => r.Orders).ThenInclude(o => o.Response)
                        .FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
                    if (user != null)
                    {
                        var responses = (from order in user.Orders
                                         where order.Response != null
                                         select order.Response).ToList();
                        var responsesRequests = (from request in user.Requests
                                                 where request.ActiveOrder != null 
                                                 && request.ActiveOrder.Response != null
                                                 select request.ActiveOrder.Response).ToList();
                        responses.AddRange(responsesRequests);
                        return View(responses);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Responses - MyResponses - Error: {ex.Message}");
                }
                return NotFound();
            }
            return NotFound();
        }

        [Authorize]
        public async Task<IActionResult> Create(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            try
            {
                Order order = await _dbContext.Orders.Include(o => o.Response).FirstOrDefaultAsync(m => m.OrderId == id && m.Response == null);
                if (order == null)
                {
                    return NotFound();
                }
                var response = new Response
                {
                    OrderId = id
                };
                return View(response);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Responses - Create - Error: {ex.Message}");
            }
            return BadRequest();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Create(Response model)
        {
            if (!User.Identity.IsAuthenticated)
                return BadRequest();
            try
            {
                var user = await _dbContext.Users.FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
                if (user == null)
                    return BadRequest();
                if (ModelState.IsValid)
                {
                    model.DateCreated = DateTime.Now;
                    var order = await _dbContext.Orders.Include(o => o.Service).ThenInclude(s => s.User)
                        .Include(o => o.Request).ThenInclude(r => r.User)
                        .Include(o => o.User)
                        .FirstOrDefaultAsync(o => o.OrderId == model.OrderId);
                    if (order == null)
                        return BadRequest();
                    if (order.ServiceId != null)
                    {
                        //for service
                        var serviceOwner = order.User;
                        user.Credits -= model.Credits + 1;
                        serviceOwner.Credits += model.Credits;
                        _dbContext.Users.Update(serviceOwner);
                    }
                    else
                    {
                        //for request
                        user.Credits += model.Credits;
                        order.Request.User.Credits -= model.Credits + 1;
                        order.Request.IsDone = true;
                        _dbContext.Requests.Update(order.Request);
                        _dbContext.Users.Update(order.Request.User);
                    }
                    order.IsDone = true;
                    _dbContext.Users.Update(user);
                    order.Credits = model.Credits;
                    _dbContext.Orders.Update(order);
                    _dbContext.Responses.Add(model);
                    if (await _dbContext.SaveChangesAsync() > 0)
                    {
                        EmailHelper emailHelper = new EmailHelper(_settings)
                        {
                            ToAddress = user.Email,
                            ToAdressTitle = $"{user.FirstName} {user.LastName}"
                        };
                        if (order.ServiceId != null)
                        {
                            var service = await _dbContext.Services.Include(s => s.Orders).ThenInclude(o => o.Response).FirstOrDefaultAsync(s => s.ServiceId == order.ServiceId);
                            CalculateRate(service);
                        }
                        model.Order = order;
                        emailHelper.SendResponseNotification(model);
                    }
                    return RedirectToAction("MyOrders", "Orders");
                }
                return View(model);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Responses - Create - Error: {ex.Message}");
            }
            return BadRequest();
        }

        private void CalculateRate(Service service)
        {
            try
            {
                var responses = from order in service.Orders
                                where order.Response != null
                                select order.Response;
                int sum = 0;
                foreach (var response in responses)
                {
                    sum += response.Rate;
                }
                double rate = (double)sum / responses.Count();
                service.Rate = Math.Round(rate, 2);
                _dbContext.Services.Update(service);
                _dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Responses - CalculateRate - Error: {ex.Message}");
            }
        }
    }
}
