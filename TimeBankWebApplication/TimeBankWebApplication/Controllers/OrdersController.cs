using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TimeBankWebApplication.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using TimeBankWebApplication.Helpers;
using Microsoft.Extensions.Options;
using System;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace TimeBankWebApplication.Controllers
{
    public class OrdersController : Controller
    {
        private ApplicationContext _dbContext;
        private IOptions<AppSettings> _settings;
        private readonly ILogger<AccountController> _logger;

        public OrdersController(ApplicationContext dbContext, IOptions<AppSettings> settings,
                                 ILogger<AccountController> logger)
        {
            _dbContext = dbContext;
            _settings = settings;
            _logger = logger;
        }

        [Authorize]
        public async Task<IActionResult> AddRequestOrder(int? id)
        {
            if (id == null)
                return BadRequest();
            try
            {
                var request = await _dbContext.Requests.FirstOrDefaultAsync(r => r.RequestId == id);

                if (request != null && User.Identity.IsAuthenticated)
                {
                    var user = await _dbContext.Users.Include(o => o.Orders).FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
                    if (user != null)
                    {
                        Order order = new Order
                        {
                            Request = request,
                            User = user,
                            RequestId = request.RequestId,
                            UserId = user.Id
                        };
                        return View(order);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Orders - AddRequestOrder - Error: {ex.Message}");
            }
            return BadRequest();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> SaveRequest(Order model)
        {
            if (!User.Identity.IsAuthenticated)
                return BadRequest();
            if (ModelState.IsValid)
            {
                try
                {
                    _dbContext.Orders.Add(model);
                    model.DateCreated = DateTime.Now;
                    model.IsActive = true;
                    if (await _dbContext.SaveChangesAsync() > 0)
                    {
                        model = await _dbContext.Orders.Include(s => s.Request).ThenInclude(c => c.Category)
                            .Include(s => s.Request).ThenInclude(u => u.User).Include(s => s.User)
                            .FirstOrDefaultAsync(s => s.OrderId == model.OrderId);
                        EmailHelper emailHelper = new EmailHelper(_settings)
                        {
                            ToAddress = model.Request.User.Email,
                            ToAdressTitle = $"{model.Request.User.FirstName} {model.Request.User.LastName}"
                        };
                        emailHelper.SendRequestOrderNotification(model);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Orders - SaveRequest - Error: {ex.Message}");
                }
                return RedirectToAction("MyOrders");
            }
            return View(model);
        }

        [Authorize]
        public async Task<IActionResult> AddServiceOrder(int? id)
        {
            if (id == null)
                return BadRequest();
            try
            {
                var service = await _dbContext.Services.FirstOrDefaultAsync(s => s.ServiceId == id);

                if (service != null && User.Identity.IsAuthenticated)
                {
                    var user = await _dbContext.Users.Include(o => o.Orders).FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
                    if (user != null)
                    {
                        Order order = new Order
                        {
                            Service = service,
                            User = user,
                            ServiceId = service.ServiceId,
                            UserId = user.Id
                        };
                        return View(order);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Orders - AddServiceOrder - Error: {ex.Message}");
            }
            return BadRequest();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> SaveService(Order model)
        {
            if (!User.Identity.IsAuthenticated)
                return BadRequest();
            if (ModelState.IsValid)
            {
                try
                {
                    model.DateCreated = DateTime.Now;
                    model.IsActive = true;
                    _dbContext.Orders.Add(model);
                    if (await _dbContext.SaveChangesAsync() > 0)
                    {
                        model = await _dbContext.Orders.Include(s => s.User)
                            .Include(s => s.Service).ThenInclude(c => c.Category)
                            .Include(s => s.Service).ThenInclude(u => u.User).Include(s => s.User)
                            .FirstOrDefaultAsync(s => s.OrderId == model.OrderId);
                        model.User.Credits--;
                        await _dbContext.SaveChangesAsync();
                        EmailHelper emailHelper = new EmailHelper(_settings)
                        {
                            ToAddress = model.Service.User.Email,
                            ToAdressTitle = $"{model.Service.User.FirstName} {model.Service.User.LastName}"
                        };
                        emailHelper.SendServiceOrderNotification(model);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Orders - SaveService - Error: {ex.Message}");
                }
                return RedirectToAction("MyOrders");
            }
            return View(model);
        }

        [Authorize]
        public async Task<IActionResult> MyOrders()
        {
            //All orders with services and requests
            if (User.Identity.IsAuthenticated)
            {
                try
                {
                    var user = await _dbContext.Users.Include(r => r.Orders).ThenInclude(o => o.Request)
                        .Include(r => r.Orders).ThenInclude(o => o.Response)
                        .Include(r => r.Orders).ThenInclude(o => o.Service).FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
                    if (user != null)
                    {
                        return View(user.Orders);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Orders - MyOrders - Error: {ex.Message}");
                }
                return NotFound();
            }
            return NotFound();
        }

        [Authorize]
        public async Task<IActionResult> DeleteRequest(int? id)
        {
            if (User.Identity.IsAuthenticated && id != null)
            {
                try
                {
                    Order order = await _dbContext.Orders.Include(o => o.Request).SingleAsync(m => m.OrderId == id && m.Request != null);
                    if (order == null)
                        return NotFound();
                    return View(order);
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Orders - DeleteRequest - Error: {ex.Message}");
                }
            }
            return NotFound();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteRequest(int id)
        {
            if (User.Identity.IsAuthenticated)
            {
                try
                {
                    Order order = await _dbContext.Orders
                        .Include(s => s.Request).ThenInclude(c => c.Category)
                        .Include(s => s.Request).ThenInclude(u => u.User).Include(s => s.User)
                        .SingleAsync(m => m.OrderId == id);
                    order.IsActive = false;
                    _dbContext.Orders.Update(order);
                    if (await _dbContext.SaveChangesAsync() > 0)
                    {
                        EmailHelper emailHelper = new EmailHelper(_settings)
                        {
                            ToAddress = order.Request.User.Email,
                            ToAdressTitle = $"{order.Request.User.FirstName} {order.Request.User.LastName}"
                        };
                        emailHelper.SendRequestOrderNotificationDelete(order);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Orders - DeleteRequest - Error: {ex.Message}");
                }
                return RedirectToAction("MyOrders");
            }
            return NotFound();
        }

        [Authorize]
        public async Task<IActionResult> DeleteService(int? id)
        {
            if (User.Identity.IsAuthenticated && id != null)
            {
                try
                {
                    Order order = await _dbContext.Orders.Include(o => o.Service).SingleAsync(m => m.OrderId == id && m.Service != null);
                    if (order == null)
                        return NotFound();
                    return View(order);
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Orders - DeleteService - Error: {ex.Message}");
                }
            }
            return NotFound();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteService(int id)
        {
            if (User.Identity.IsAuthenticated)
            {
                try
                {
                    Order order = await _dbContext.Orders
                        .Include(o => o.User)
                        .Include(s => s.Service).ThenInclude(c => c.Category)
                        .Include(s => s.Service).ThenInclude(u => u.User).Include(s => s.User)
                        .SingleAsync(m => m.OrderId == id);
                    order.IsActive = false;
                    order.User.Credits++;
                    _dbContext.Orders.Update(order);
                    if (await _dbContext.SaveChangesAsync() > 0)
                    {
                        EmailHelper emailHelper = new EmailHelper(_settings)
                        {
                            ToAddress = order.Service.User.Email,
                            ToAdressTitle = $"{order.Service.User.FirstName} {order.Service.User.LastName}"
                        };
                        emailHelper.SendServiceOrderNotificationDelete(order);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Orders - DeleteService - Error: {ex.Message}");
                }
                return RedirectToAction("MyOrders");
            }
            return NotFound();
        }

        public async Task<IActionResult> Category(string id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            //All orders with services and requests
            if (User.Identity.IsAuthenticated)
            {
                try
                {
                    var user = await _dbContext.Users.Include(r => r.Orders).ThenInclude(o => o.Request)
                        .Include(r => r.Orders).ThenInclude(o => o.Response)
                        .Include(r => r.Orders).ThenInclude(o => o.Service).FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
                    if (user != null)
                    {
                        var orders = user.Orders;
                        switch (id)
                        {
                            case "orders-services":
                                orders = orders.Where(o => o.ServiceId != null).ToList();
                                break;
                            case "orders-requests":
                                orders = orders.Where(o => o.RequestId != null).ToList();
                                break;
                            case "orders-active":
                                orders = orders.Where(o => o.IsActive).ToList();
                                break;
                            case "orders-notactive":
                                orders = orders.Where(o => !o.IsActive).ToList();
                                break;
                            case "orders-isdone":
                                orders = orders.Where(o => o.IsDone).ToList();
                                break;
                            case "orders-isnotdone":
                                orders = orders.Where(o => !o.IsDone).ToList();
                                break;
                        }
                        return PartialView("OrdersPartialView", orders);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Orders - Category - Error: {ex.Message}");
                }
                return NotFound();
            }
            return NotFound();
        }
    }
}