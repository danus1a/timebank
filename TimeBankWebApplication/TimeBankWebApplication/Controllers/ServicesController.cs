using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using TimeBankWebApplication.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.IO;
using Microsoft.Net.Http.Headers;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using TimeBankWebApplication.Helpers;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;

namespace TimeBankWebApplication.Controllers
{
    public class ServicesController : Controller
    {
        private const int _itemsOnPage = 8;
        private const int _pagesOnPagination = 5;
        private ApplicationContext _dbContext;
        private string[] _allowedExtensions;
        private IHostingEnvironment _environment;
        private IOptions<AppSettings> _settings;
        private readonly ILogger<ServicesController> _logger;

        public ServicesController(ApplicationContext dbContext, IHostingEnvironment environment,
            IOptions<AppSettings> settings, ILogger<ServicesController> logger)
        {
            _dbContext = dbContext;
            _environment = environment;
            _allowedExtensions = new[] { ".jpg", ".png"};
            _settings = settings;
            _logger = logger;
        }

        public async Task<IActionResult> AllServices(int? page)
        {
            List<Service> services = new List<Service>();
            List<User> users = new List<User>();
            try
            {
                if (User.Identity.IsAuthenticated)
                {
                    users = await _dbContext.Users.Include(u => u.Services)
                        .ThenInclude(s => s.Category).Where(u => u.UserName != User.Identity.Name).ToListAsync();
                }
                else
                    users = await _dbContext.Users.Include(u => u.Services)
                        .ThenInclude(s => s.Category).ToListAsync();
                services = (from user in users
                            from service in user.Services
                            where service.IsActive
                            select service).ToList();
            }
            catch(Exception ex)
            {
                _logger.LogError($"Services - AllServices - Error: {ex.Message}");
            }
            return GetPagination(page, services, "AllServices", null, false, "AllServices");
        }

        public async Task<IActionResult> Category(int? id, int? page)
        {
            if (id == null)
            {
                return BadRequest();
            }
            List<Service> services = new List<Service>();
            List<User> users = new List<User>();
            try
            {
                var category = _dbContext.Categories.FirstOrDefaultAsync(c => c.CategoryId == id).Result;
                if (category == null && id != 0)
                {
                    return BadRequest();
                }
                if (User.Identity.IsAuthenticated)
                {
                    users = await _dbContext.Users.Include(u => u.Services)
                        .ThenInclude(s => s.Category)
                        .Where(u => u.UserName != User.Identity.Name).ToListAsync();
                }
                else
                    users = await _dbContext.Users.Include(u => u.Services)
                        .ThenInclude(s => s.Category)
                        .ToListAsync();
                services = (from user in users
                            from service in user.Services
                            where service.IsActive
                            select service).ToList();
                if (id > 0)
                    services = services.Where(s => s.CategoryId == id).ToList();
                ViewBag.Title = id > 0 ? category.Name : "All";
                ViewBag.Category = category.Name;
                ViewBag.CategoryId = id;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Services - AllServices - Error: {ex.Message}");
            }
            return GetPagination(page, services, "AllServices", id.ToString(), true, "ServicesPartialView");
        }

        [Authorize]
        public async Task<IActionResult> MyServices()
        {
            if (User.Identity.IsAuthenticated)
            {
                try
                {
                    var user = await _dbContext.Users.Include(u => u.Services)
                        .ThenInclude(s => s.Category).FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
                    if (user != null)
                    {
                        return View(user.Services);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Services - MyServices - Error: {ex.Message}");
                }
                return NotFound();
            }
            return NotFound();
        }

        [Authorize]
        public async Task<IActionResult> GetService(int? id)
        {
            if (id == null)
                return BadRequest();
            Service service = null;
            User user = null;
            try
            {
                user = await _dbContext.Users.FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
                service = await _dbContext.Services.Include(s => s.Category).Include(s => s.User)
                    .Include(s => s.Orders).ThenInclude(o => o.Response).FirstOrDefaultAsync(r => r.ServiceId == id);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Services - AllServices - Error: {ex.Message}");
            }
            if (service != null && user != null)
            {
                ViewBag.CurrentUserId = user.Id;
                return View(service);
            }
            return BadRequest();
        }

        [Authorize]
        public IActionResult Add()
        {
            ViewData["CategoryId"] = GetCategoriesSelectList();
            return View();
        }

        private List<SelectListItem> GetCategoriesSelectList(string selectedValue = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            try
            {
                list = _dbContext.Categories.Select(item => new SelectListItem
                {
                    Value = item.CategoryId.ToString(),
                    Text = item.Name,
                    Selected = item.CategoryId.ToString() == selectedValue
                }).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Services - GetCategoriesSelectList - Error: {ex.Message}");
            }
            return list;
        }

        private bool VerifyFileExtension(string path)
        {
            return _allowedExtensions.Contains(Path.GetExtension(path));
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Add(Service serviceModel)
        {
            if (!User.Identity.IsAuthenticated)
                return BadRequest();
            User user = null;
            try
            {
                user = await _dbContext.Users.FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Services - Add - Error: {ex.Message}");
            }
            if (user == null)
                return BadRequest();
            if (ModelState.IsValid)
            {
                var fileName = "";
                if (serviceModel.Image != null && serviceModel.Image.Length > 0)
                {
                    try
                    {
                        fileName =
                            ContentDispositionHeaderValue.Parse(serviceModel.Image.ContentDisposition).FileName.Trim('"');
                        if (VerifyFileExtension(fileName))
                        {
                            var uploads = Path.Combine(_environment.WebRootPath, "images");

                            var filePath = Path.Combine(uploads, fileName);
                            using (var fileStream = new FileStream(filePath, FileMode.Create))
                            {
                                serviceModel.Image.CopyTo(fileStream);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("Image",
                                $"Image can be only with extensions: {_allowedExtensions.Join()}.");
                            ViewData["CategoryId"] = GetCategoriesSelectList(serviceModel.CategoryId.ToString());
                            return View(serviceModel);
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"Services - Add(saving image) - Error: {ex.Message}");
                    }
                }
                try
                {
                    var service = new Service
                    {
                        Caption = serviceModel.Caption,
                        Credits = serviceModel.Credits,
                        DateCreated = DateTime.Now,
                        Description = serviceModel.Description,
                        UserId = user.Id,
                        CategoryId = serviceModel.CategoryId,
                        ImageUrl = fileName == "" ? "/images/patterns/Services-Pattern.jpg" : $"/images/{fileName}",
                        IsActive = true
                    };
                    _dbContext.Services.Add(service);
                    if (await _dbContext.SaveChangesAsync() > 0)
                    {
                        service = await _dbContext.Services.Include(s => s.Category).FirstOrDefaultAsync(s => s.ServiceId == service.ServiceId);
                        EmailHelper emailHelper = new EmailHelper(_settings)
                        {
                            ToAddress = user.Email,
                            ToAdressTitle = $"{user.FirstName} {user.LastName}"
                        };
                        emailHelper.SendServiceNotification(service);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Services - Add(save) - Error: {ex.Message}");
                }
                return RedirectToAction("MyServices");
            }
            ViewData["CategoryId"] = GetCategoriesSelectList(serviceModel.CategoryId.ToString());
            return View(serviceModel);
        }

        [Authorize]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();
            Service service = null;
            try
            {
                service = await _dbContext.Services.SingleAsync(m => m.ServiceId == id);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Services - Edit - Error: {ex.Message}");
            }
            if (service == null)
                return NotFound();
            ViewData["CategoryId"] = GetCategoriesSelectList(service.CategoryId.ToString());
            return View(service);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Service serviceModel)
        {
            Service service = null;
            try
            {
                service = await _dbContext.Services.SingleAsync(m => m.ServiceId == serviceModel.ServiceId);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Services - Edit - Error: {ex.Message}");
            }
            if (service == null)
                return NotFound();
            if (ModelState.IsValid)
            {
                var fileName = "";
                if (serviceModel.Image != null && serviceModel.Image.Length > 0)
                {
                    try
                    {
                        fileName = ContentDispositionHeaderValue.Parse(serviceModel.Image.ContentDisposition).FileName.Trim('"');
                        if (VerifyFileExtension(fileName))
                        {
                            var uploads = Path.Combine(_environment.WebRootPath, "images");

                            var filePath = Path.Combine(uploads, fileName);
                            using (var fileStream = new FileStream(filePath, FileMode.Create))
                            {
                                await serviceModel.Image.CopyToAsync(fileStream);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("Image",
                                $"Image can be only with extensions: {_allowedExtensions.Join()}.");
                            ViewData["CategoryId"] = GetCategoriesSelectList(service.CategoryId.ToString());
                            return View(serviceModel);
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"Services - Edit(save image) - Error: {ex.Message}");
                    }
                }
                service.Caption = serviceModel.Caption;
                service.Credits = serviceModel.Credits;
                service.Description = serviceModel.Description;
                service.CategoryId = serviceModel.CategoryId;                    
                if (fileName != "")
                    service.ImageUrl = $"/images/{fileName}";
                try
                {
                    _dbContext.Update(service);
                    await _dbContext.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Services - Edit - Error: {ex.Message}");
                }
                return RedirectToAction("MyServices");
            }
            ViewData["CategoryId"] = GetCategoriesSelectList(service.CategoryId.ToString());
            return View("Edit", serviceModel);
        }

        [Authorize]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();
            Service service = null;
            try
            {
                service = await _dbContext.Services.SingleAsync(m => m.ServiceId == id);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Services - Delete - Error: {ex.Message}");
            }
            if (service == null)
                return NotFound();
            return View(service);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            Service service = null;
            try
            {
                service = await _dbContext.Services.SingleAsync(m => m.ServiceId == id);
                service.IsActive = false;
                _dbContext.Services.Update(service);
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Services - Delete - Error: {ex.Message}");
            }
            return RedirectToAction("MyServices");
        }

        [Authorize]
        public async Task<IActionResult> GetResponses(int? id)
        {
            if (id == null)
                return NotFound();
            Service service = null;
            try
            {
                service = await _dbContext.Services.Include(s => s.Orders).ThenInclude(o => o.Response)
                    .SingleAsync(m => m.ServiceId == id);
                if (service == null)
                    return NotFound();
                var responses = (from order in service.Orders
                                 select order.Response).ToList();
                if (responses.Any())
                    return View(service);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Services - GetResponses - Error: {ex.Message}");
            }
            return NotFound();
        }

        private void RenderPagination(int? page, string action, string routeCategory, decimal pagesCount)
        {
            var pager = new Pager
            {
                PreviousPage = page.Value >= 2 ? (page.Value - 1).ToString() : "",
                NextPage = page.Value < pagesCount ? (page.Value + 1).ToString() : "",
                CurrentPage = page.Value.ToString(),
                Action = action,
                RouteCategory = routeCategory,
                Controller = "Services"
            };
            //current page
            var pagesList = new List<string> { page.Value.ToString() };
            if (page.Value == 1)
            {
                for (var i = page.Value + 1; i <= _pagesOnPagination; i++)
                {
                    if (i > pagesCount)
                        continue;
                    pagesList.Add(i.ToString());
                }
            }
            else if (page.Value == 2)
            {
                pagesList.Add((page.Value - 1).ToString());
                for (var i = page.Value + 1; i <= _pagesOnPagination; i++)
                {
                    if (i > pagesCount)
                        continue;
                    pagesList.Add(i.ToString());
                }
            }
            else if (page.Value == pagesCount)
            {
                for (var i = page.Value - 1; i > 0; i--)
                {
                    if (pagesList.Count == _pagesOnPagination)
                        continue;
                    pagesList.Add(i.ToString());
                }
            }
            else if (page.Value == pagesCount - 1)
            {
                pagesList.Add(pagesCount.ToString());
                for (var i = page.Value - 1; i > 0; i--)
                {
                    if (pagesList.Count == _pagesOnPagination)
                        continue;
                    pagesList.Add(i.ToString());
                }
            }
            else if (page.Value == pagesCount - 2)
            {
                pagesList.Add(pagesCount.ToString());
                pagesList.Add((pagesCount - 1).ToString());
                for (var i = page.Value - 1; i > 0; i--)
                {
                    if (pagesList.Count == _pagesOnPagination)
                        continue;
                    pagesList.Add(i.ToString());
                }
            }
            else
            {
                for (var i = 1; i <= _pagesOnPagination; i++)
                {
                    if (i != page.Value)
                        pagesList.Add(i.ToString());
                }
            }
            pager.Pages = pagesList.OrderBy(item => item).ToList();
            ViewBag.pager = pager;
        }

        private IActionResult GetPagination(int? page, List<Service> services, string action, string routeCategory, bool isPartial, string viewName)
        {
            if (page == null || page.Value <= 0)
                page = 1;
            decimal pagesCount = Math.Ceiling((decimal)services.Count / _itemsOnPage);
            RenderPagination(page, action, routeCategory, pagesCount);
            if (!services.Any())
            {
                if (isPartial)
                    return PartialView(viewName, services);
                return View(viewName, services);
            }
            if (isPartial)
                return PartialView(viewName, services.Skip((page.Value - 1) * _itemsOnPage).Take(_itemsOnPage));
            return View(viewName, services.Skip((page.Value - 1) * _itemsOnPage).Take(_itemsOnPage));
        }

        public IActionResult PaginationViewComponent(int? page, string action, int routeCategory)
        {
            if (page == null || page.Value <= 0)
                page = 1;
            List<Service> services = new List<Service>();
            List<User> users = new List<User>();
            try
            {
                if (User.Identity.IsAuthenticated)
                {
                    users = _dbContext.Users.Include(u => u.Services).Where(u => u.UserName != User.Identity.Name).ToList();
                }
                else
                    users = _dbContext.Users.Include(u => u.Services).ToList();
                services = (from user in users
                            from service in user.Services
                            where service.IsActive 
                            select service).ToList();
                if (routeCategory > 0)
                    services = services.Where(s => s.CategoryId == routeCategory).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Services - PaginationViewComponent - Error: {ex.Message}");
            }
            decimal pagesCount = Math.Ceiling((decimal)services.Count / _itemsOnPage);
            if (page.Value > pagesCount)
                return BadRequest();
            RenderPagination(page, action, routeCategory.ToString(), pagesCount);
            return ViewComponent("Pagination", (Pager)ViewBag.pager);
        }
    }
}