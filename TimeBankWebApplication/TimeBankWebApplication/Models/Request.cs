﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace TimeBankWebApplication.Models
{
    public class Request
    {
        public int RequestId { get; set; }

        public int UserId { get; set; }

        [Required(ErrorMessage = "Category is required")]
        [Display(Name = "Category")]
        public int CategoryId { get; set; }

        [Required(ErrorMessage = "Caption is required")]
        [MinLength(10, ErrorMessage = "Minimum length is 10")]
        [MaxLength(100, ErrorMessage = "Maximum length is 100")]
        public string Caption { get; set; }

        [Required(ErrorMessage = "Description is required")]
        [MinLength(10, ErrorMessage = "Minimum length is 10")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Credits count is required")]
        [Range(1, 100, ErrorMessage = "Credits count can be from 1 to 100")]
        [Display(Name = "Credits count")]
        public int Credits { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateDone { get; set; }

        [Display(Name = "End date")]
        [Required(ErrorMessage = "End date")]
        [DataType(DataType.DateTime)]
        public DateTime DateEnd { get; set; }

        public bool IsDone { get; set; }

        [Display(Name = "Is active")]
        public bool IsActive { get; set; }

        [Display(Name = "Why is not active")]
        [MinLength(10, ErrorMessage = "The minimum length is 10")]
        public string OffReason { get; set; }

        //Navigation properties
        public virtual List<Order> Orders { get; set; }
        public User User { get; set; }
        public Category Category { get; set; }
        [NotMapped]
        public Order ActiveOrder
        {
            get
            {
                return Orders != null ? Orders.FirstOrDefault(o => o.IsActive) : null;
            }
        }
    }
}
