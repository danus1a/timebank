﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace TimeBankWebApplication.Models
{
    public class User : IdentityUser<int>
    {
        [Required(ErrorMessage = "First Name is required")]
        [MinLength(5, ErrorMessage = "Minimum length is 5")]
        [MaxLength(50, ErrorMessage = "Minimum length is 50")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last Name is required")]
        [MinLength(5, ErrorMessage = "Minimum length is 5")]
        [MaxLength(50, ErrorMessage = "Minimum length is 50")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [MinLength(5, ErrorMessage = "Minimum length is 5")]
        [MaxLength(50, ErrorMessage = "Minimum length is 50")]
        public string Phone { get; set; }

        [MinLength(10, ErrorMessage = "Minimum length is 10")]
        public string Address { get; set; }

        public int Credits { get; set; }

        public virtual List<Service> Services { get; set; }
        public virtual List<Request> Requests { get; set; }
        public virtual List<Order> Orders { get; set; }

        public bool IsActiveServiceOrder (int? serviceId)
        {
            if (serviceId == null)
                return false;
            return Orders.Any(o => o.ServiceId == serviceId && o.IsActive && !o.IsDone);
        }
    }

    public class Role : IdentityRole<int>
    {

    }
}
