﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace TimeBankWebApplication.Models
{
    public class ApplicationContext : IdentityDbContext<User, Role, int>
    {
        public ApplicationContext() { }

        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            //builder.UseSqlServer("Server=tcp:timebankwebapplication20170602030651dbserver.database.windows.net,1433;Initial Catalog=TimeBankWebApplication20170602030651_db;Persist Security Info=False;User ID=bodyadana;Password=Password1!;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            builder.UseSqlServer("Server=DANA\\SQLEXPRESS;Database=TimeBank;Trusted_Connection=True;MultipleActiveResultSets=True");
            base.OnConfiguring(builder);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<User>(entity =>
            {
                entity.ToTable(name: "AspNetUser", schema: "Security");
            });

            builder.Entity<Role>(entity =>
            {
                entity.ToTable(name: "AspNetRole", schema: "Security");
            });

            builder.Entity<IdentityUserClaim<int>>(entity =>
            {
                entity.ToTable("AspNetUserClaim", "Security");
            });

            builder.Entity<IdentityUserLogin<int>>(entity =>
            {
                entity.ToTable("AspNetUserLogin", "Security");
            });

            builder.Entity<IdentityRoleClaim<int>>(entity =>
            {
                entity.ToTable("AspNetRoleClaim", "Security");
            });

            builder.Entity<IdentityUserRole<int>>(entity =>
            {
                entity.ToTable("AspNetUserRole", "Security");
            });

            builder.Entity<IdentityUserToken<int>>(entity =>
            {
                entity.ToTable("AspNetUserToken", "Security");
            });
        }

        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<Response> Responses { get; set; }
        public virtual DbSet<Request> Requests { get; set; }
    }
}
