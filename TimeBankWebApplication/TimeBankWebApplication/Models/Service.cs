﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TimeBankWebApplication.Models
{
    public class Service
    {
        public int ServiceId { get; set; }
        
        public int UserId { get; set; }

        [Required(ErrorMessage = "Category is required")]
        [Display(Name = "Category")]
        public int CategoryId { get; set; }

        [Required(ErrorMessage = "Caption is required")]
        [MinLength(10, ErrorMessage = "Minimum length is 10")]
        [MaxLength(100, ErrorMessage = "Minimum length is 100")]
        public string Caption { get; set; }

        [Required(ErrorMessage = "Description is required")]
        [MinLength(10, ErrorMessage = "Minimum length is 10")]
        public string Description { get; set; }

        [MaxLength(250)]
        public string ImageUrl { get; set; }

        [NotMapped]
        public IFormFile Image { get; set; }

        [Required(ErrorMessage = "Credits number is required")]
        [Range(1, 100, ErrorMessage = "Credits can be from 1 to 100")]
        [Display(Name = "Credits count")]
        public int Credits { get; set; }

        public DateTime DateCreated { get; set; }

        public double? Rate { get; set; }

        [Display(Name = "Is active")]
        public bool IsActive { get; set; }

        [Display(Name = "Why is not active")]
        [MinLength(10, ErrorMessage = "The minimum length is 10")]
        public string OffReason { get; set; }

        //Navigation properties
        public virtual List<Order> Orders { get; set; }
        public User User { get; set; }
        public Category Category { get; set; }
    }
}
