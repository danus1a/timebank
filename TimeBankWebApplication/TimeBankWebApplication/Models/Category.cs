﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TimeBankWebApplication.Models
{
    public class Category
    {
        public int CategoryId { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [MinLength(3, ErrorMessage = "The minimum length is 3")]
        [MaxLength(50, ErrorMessage = "The maximum length is 50")]
        public string Name { get; set; }

        public virtual List<Service> Services { get; set; }
        public virtual List<Request> Requests { get; set; }
    }
}
