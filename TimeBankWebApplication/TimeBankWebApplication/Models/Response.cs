﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TimeBankWebApplication.Models
{
    public class Response
    {
        public int ResponseId { get; set; }

        public int? OrderId { get; set; }

        [Required(ErrorMessage = "Caption is required")]
        [MinLength(10, ErrorMessage = "Minimum length is 10")]
        [MaxLength(50, ErrorMessage = "Minimum length is 50")]
        public string Caption { get; set; }

        [Required(ErrorMessage = "Description is required")]
        [MinLength(10, ErrorMessage = "Minimum length is 10")]
        public string Description { get; set; }
        
        [Required(ErrorMessage = "Rate is required")]
        [Range(1,5, ErrorMessage = "Rate can be from 1 to 5")]
        public int Rate { get; set; }

        [Required(ErrorMessage = "Credits number is required")]
        [Range(1, 100, ErrorMessage = "Credits number can be from 1 to 100 hours")]
        public int Credits { get; set; }

        public DateTime DateCreated { get; set; }

        [NotMapped]
        public List<int> Rates { get { return new List<int> { 1, 2, 3, 4, 5 }; } }

        //Navigation properties
        public virtual Order Order { get; set; }
    }
}
