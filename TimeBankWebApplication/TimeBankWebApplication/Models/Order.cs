﻿using System;

namespace TimeBankWebApplication.Models
{
    public class Order
    {
        public int OrderId { get; set; }

        public int UserId { get; set; }

        public int? ServiceId { get; set; }

        public int? RequestId { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateDone { get; set; }
        
        public int Credits { get; set; }

        public bool IsActive { get; set; }

        public bool IsDone { get; set; }

        //Navigation properties
        public virtual Request Request { get; set; }
        public virtual Service Service { get; set; }
        public virtual Response Response { get; set; }
        public User User { get; set; }
    }
}
