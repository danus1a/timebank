﻿namespace TimeBankWebApplication.Models
{
    public class AppSettings
    {
        public string AdminEmail { get; set; }
        public string ConnectionString { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
