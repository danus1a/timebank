﻿using System.ComponentModel.DataAnnotations;

namespace TimeBankWebApplication.ViewModels
{
    public class UserViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "First name is required")]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last name is required")]
        [Display(Name = "Last name")]
        public string LastName { get; set; }


        [Required(ErrorMessage = "Phone is required")]
        [MinLength(5, ErrorMessage = "Minimum length is 5")]
        [MaxLength(50, ErrorMessage = "Minimum length is 50")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Address is required")]
        [MinLength(10, ErrorMessage = "Minimum length is 10")]
        public string Address { get; set; }
    }
}