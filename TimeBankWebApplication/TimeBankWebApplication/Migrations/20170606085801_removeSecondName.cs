﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TimeBankWebApplication.Migrations
{
    public partial class removeSecondName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SecondName",
                schema: "Security",
                table: "AspNetUser");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SecondName",
                schema: "Security",
                table: "AspNetUser",
                maxLength: 50,
                nullable: true);
        }
    }
}
